const buttons = document.querySelectorAll('article');
const overlayImage = document.querySelectorAll('.overlay img');
const titre = document.querySelectorAll('.titre');
const filterNode = document.querySelectorAll('ul li');
const filterDesc = document.querySelectorAll('.description');

// Get the user-agent string
let userAgentString = navigator.userAgent;
// Detect Chrome
let chromeAgent = userAgentString.indexOf("Chrome") > -1;
console.log(chromeAgent);
let overlayNode = document.querySelectorAll('.overlay');
let overlay = [];
let filter = [];


function ProjectCSS(prj) {
  // ! Applique le CSS adéquat pour le naviguateur Chrome lors du chargement de la page
  if (prj.length <= 8 && chromeAgent == true) { 
    document.querySelector('footer').style.setProperty('position', 'absolute');
    document.querySelector('body').style.setProperty('height', '100%');
    document.querySelector('html').style.setProperty('height', '100%');
    
  } else{
    document.querySelector('footer').style.setProperty('position', 'unset');
    document.querySelector('footer').style.setProperty('margin-top', '50px');
    document.querySelector('body').style.setProperty('height', 'unset');
    document.querySelector('html').style.setProperty('height', 'unset');
  }
}
ProjectCSS(buttons);

function OverlayValues(array) {
  overlayNode.forEach(element => {
    // console.log(element);
    // console.log(element.querySelector('h3').innerHTML);
      array.push(element.querySelector('h3').innerHTML);
  });
}
OverlayValues(overlay);

function FilterValues(array) {
  filterNode.forEach(element => {
    array.push(element.innerText);
  });
}
FilterValues(filter);

buttons.forEach(button => button.addEventListener('click', open));

filterNode.forEach(button => button.addEventListener('click', filtration));

function open(e) {
  window.scrollTo({
    top: 0,
    left: 0,
    behavior: 'smooth'
  });
  let currentValue = e.currentTarget.querySelector('h3').innerText;
  
  if (overlay.includes(currentValue)) {
    let goodOverlay = overlay.indexOf(currentValue);
    let popup = document.getElementById(goodOverlay);
    let background = document.querySelector('.modal-overlay')
    background.classList.add('state-show');
    popup.classList.add('open');
    const src = e.currentTarget.querySelector('img').src;
    nodeItem = overlayImage.item(goodOverlay)
    // console.log(nodeItem,src);
    nodeItem.src = src;
    close(popup,background,nodeItem);
  } else {
    // console.error("Le projet " +currentValue+ " n'a pas de popup correspondante 😱😱 vérifié si : \n ❌ ID manquant 😭 \n ❌ Un mauvais titre de popup 😭 \n ❌ Le titre du projet correspond au titre de popup 😭");
  }
}

function close(popup,background,Image) {
  const btnClose = document.querySelectorAll('.close')
  btnClose.forEach(button => button.addEventListener('click',(e) => {
    Image.removeAttribute('src');
    background.classList.remove('state-show');
    popup.classList.remove('open');
  }));
}

function filtration(e) {
  let currentFilter = e.currentTarget.innerText;
  filterNode.forEach(element => {
    element.classList.remove('active');
  });
  
  e.currentTarget.classList.add('active');
  let eachProject = [];
  let prj = []
  filterDesc.forEach(element => {
    eachProject.push(element.querySelector('p').innerText);
  });
  
  if (currentFilter == 'Tous') {
    // ! Applique le CSS adéquat pour le naviguateur Chrome lors de la selection du filtre Tous
    if (buttons.length <= 8 && chromeAgent == true ) {
      document.querySelector('footer').style.setProperty('position', 'absolute');
      document.querySelector('body').style.setProperty('height', '100%');
      document.querySelector('html').style.setProperty('height', '100%');

    } else{
      document.querySelector('footer').style.setProperty('position', 'unset');
      document.querySelector('footer').style.setProperty('margin-top', '50px');
      document.querySelector('body').style.setProperty('height', 'unset');
      document.querySelector('html').style.setProperty('height', 'unset');
    }
    buttons.forEach(element => {
      element.style.setProperty('display', 'flex');
    });
  } else if (eachProject.some(projet => projet.includes(currentFilter))) {
    buttons.forEach(element => {
      let projet = element.querySelector('p').innerText;

      if (projet === currentFilter || projet.includes(currentFilter)) {
        prj.push(projet);
        element.style.setProperty('display', 'flex');
        if (prj.length < 4 && chromeAgent == true ) {
          console.log(true, "prj.length <= 8 && chromeAgent == true ");
          document.querySelector('footer').style.setProperty('position', 'absolute');
          document.querySelector('body').style.setProperty('height', '100%');
          document.querySelector('html').style.setProperty('height', '100%');

        } else if (prj.length < 4 && chromeAgent == false ) {
          console.log(true, "prj.length < 4 && chromeAgent == false ");
          document.querySelector('footer').style.setProperty('position', 'absolute');
          document.querySelector('body').style.setProperty('height', '100%');
          document.querySelector('html').style.setProperty('height', '100%');

        } else{
          document.querySelector('footer').style.setProperty('position', 'unset');
          document.querySelector('footer').style.setProperty('margin-top', '50px');
          document.querySelector('body').style.setProperty('height', 'unset');
          document.querySelector('html').style.setProperty('height', 'unset');
        }
      } else {
        element.style.setProperty('display', 'none');
      }
      
    });
  } else{
    // console.error("Erreur lors de la filtration correspondante 😱😱 vérifié si : \n ❌ Le filtre existe 😭 \n ❌ Une erreur orthographique 😭");
  }
}



 
