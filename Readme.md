# Mon portefolio

[![forthebadge](https://forthebadge.com/images/badges/uses-html.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/uses-js.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/uses-css.svg)](https://forthebadge.com)

Ce projet resencera et présentera tout mes projets devellopeur et maker.

## Un nouveau projet ? 🧾
    0️⃣ Dupliquer un article existant ou créer en un
    1️⃣ Dans la description, il faut s'assurer de trois choses : 
      ➡️ Mettre un titre
      ➡️ Mettre un type (pour le filtre)
      ➡️ Un bouton
    2️⃣ Ajouter une image
    3️⃣ Dupliquer la div overlay existante ou créer en une
    4️⃣ Attribuer un nouvel ID à la div, puis à l'interieur : 
      ➡️ Mettre le même titre que l'article
      ➡️ Le reste est libre de modification
      ➡️ <img> rapellera l'image de l'article

## [![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)

* [Google font](https://fonts.google.com/) - Font 
* [Font Awesome](https://fontawesome.com/) - Font 
* [VS code](https://code.visualstudio.com/) - Editeur de textes
* [Jquery](https://code.jquery.com/jquery-3.5.1.slim.min.js) - Framework JS (front-end)
* [formspree](https://formspree.io/) - Editeur de Formulaire


## Versions

**Dernière version stable :** 0.0.0
**Dernière version :** 1.0

  -   Liste des versions : 
  -   [Cliquer pour afficher](https://gitlab.com/oblivion_rony/portefolio/v0.0.0)



