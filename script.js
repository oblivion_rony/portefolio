// ------------VARIABLES

let $loader = document.querySelector('.loader')
let urlcourante = document.location.pathname;
let urlindex = '/index.html';
let urlindex2 = '/';
let urlmaker = '/maker.html';
let urldev = '/developpeur.html';

// ------------CHARGEMENT INTERACTIF

window.onload = function() {
  $loader.classList.remove('loader--active')
};


// --------------LANCEMENT DU CHOIX SUR INDEX

if (urlcourante == urlindex || urlcourante == urlindex2) {

        // ------------BANIERE INTERACTIVE V2
    const left = document.querySelector(".left");
    const right = document.querySelector(".right");
    const container = document.querySelector(".container");

    left.addEventListener("mouseenter", () => {
      container.classList.add("hover-left");
    });

    left.addEventListener("mouseleave", () => {
      container.classList.remove("hover-left");
    });

    right.addEventListener("mouseenter", () => {
      container.classList.add("hover-right");
    });

    right.addEventListener("mouseleave", () => {
      container.classList.remove("hover-right");
    });

    document.querySelector('#left').addEventListener('click', function () {
      $loader.classList.add('loader--active')
      
      window.setTimeout(function () {
        $loader.classList.remove('loader--active');
      }, 2000)
      window.setTimeout(function () {
        window.location.assign ("maker.html")
      }, 2000)
    })
    document.querySelector('#right').addEventListener('click', function () {
      $loader.classList.add('loader--active')
      
      window.setTimeout(function () {
        $loader.classList.remove('loader--active');
      }, 2000)
      window.setTimeout(function () {
        window.location.assign ("developpeur.html")
      }, 2000)
    })
  } else {
    console.log('je ne suis pas sur index !')//DEBUG
  }


if (urlcourante == urldev || urlcourante == urlmaker) {
  document.querySelector('.logo').addEventListener('click', function () {
    $loader.classList.add('loader--active')
    
    window.setTimeout(function () {
      $loader.classList.remove('loader--active');
    }, 4000)
    window.setTimeout(function () {
      window.location.assign (urlindex)
    }, 4000)
  })
  }else{
    console.log(urlcourante)
  }    

// ------------CONTACT ME

  window.formbutton=window.formbutton||function(){(formbutton.q=formbutton.q||[]).push(arguments)};
  formbutton("create", 
  {action: "https://formspree.io/f/mjvpjwjd",
  buttonImg: "<i class='fas fa-envelope' style='font-size:24px'></i>",
  title: "Envoyer votre message ✉️", 
  fields: [{
    type: "text",
    placeholder: "Votre nom",
    required: true,
  },
  {
    type: "email",
    placeholder: "Votre email",
    required: true
  },
  {
    placeholder: "Votre message",
    type: "textarea",
    required: true
  },
  {
    type: "submit"
  }],
  styles: {
    button: {
      background: "#C4001A"
    },
    title: {
      background: "#C4001A",
      letterSpacing: "0.05em",
      textTransform: "uppercase"
    }
  },
  })
